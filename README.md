# Ember Mug CLI #

A simple Python script for interacting with the Ember Mug over Bluetooth.

Uses a fair bit of code from https://github.com/pledi/EmberControl which I couldn't get to work because of issues with pythonnet on Windows.

Another useful source of data was https://github.com/sopelj/hass-ember-mug-component/blob/main/custom_components/ember_mug/const.py

So I gave up and stripped out all the QT bits and just left the basic parts. To use you'll need the Bluetooth address of your mug.

# Requirements

You'll need the 'bleak' library:

pip install --user bleak