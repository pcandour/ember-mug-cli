# This Python file uses the following encoding: utf-8
import sys
import asyncio
from bleak import BleakClient, BleakScanner

CURRENT_TEMP = "fc540002-236c-4c94-8fa9-944a3e5353fa"
TARGET_TEMP  = "fc540003-236c-4c94-8fa9-944a3e5353fa"
LIQUID_LEVEL = "fc540005-236c-4c94-8fa9-944a3e5353fa"
CURRENT_BATT = "fc540007-236c-4c94-8fa9-944a3e5353fa"
LIQUID_STATE = "fc540008-236c-4c94-8fa9-944a3e5353fa"
LED_COLOR_UUID = "fc540014-236c-4c94-8fa9-944a3e5353fa"
LIQUID_STATE_LABELS = {
    0: "Unknown",
    1: "Empty",
    2: "Filling",
    3: "Cold (No control)",
    4: "Cooling",
    5: "Heating",
    6: "At Target",
    7: "Warm (No control)",
}

async def searchForMug():
    print("No device address specified. Scanning...")
    scanner = BleakScanner()
    await scanner.start()
    await asyncio.sleep(5.0)
    await scanner.stop()
    devices = await scanner.get_discovered_devices()

    print("Found %d devices." % len(devices))
    for device in devices:
        if device.name == "Ember Ceramic Mug":
            print("Ember Mug device address: %s" % device.address)
            print("Tip: Use the device address as the first argument to save having to scan again.")
            return device.address

    return None

async def checkConnected(client):
    connected = await client.is_connected()
    while not connected:
        await asyncio.sleep(1.0)
        connected = await client.is_connected()
        print("Connected: %s" % connected)

    paired = await client.pair()
    while not paired:
        await asyncio.sleep(1.0)
        paired = await client.pair()
        print("Paired: %s" % paired)

async def getLiquidState(client):
    await checkConnected(client)
    state = await client.read_gatt_char(LIQUID_STATE)
    return LIQUID_STATE_LABELS[int(state[0])]

async def getLiquidLevel(client):
    await checkConnected(client)
    level = await client.read_gatt_char(LIQUID_LEVEL)
    return int((int(level[0]) / 30) * 100)

async def getLED(client):
    await checkConnected(client)
    return await client.read_gatt_char(LED_COLOR_UUID)

async def setLED(client, color):
    #    bytearray(b'\x00\xff\x00\xff')
    await checkConnected(client)
    await client.write_gatt_char(LED_COLOR_UUID, color, False)

async def getBattery(client):
    await checkConnected(client)
    battery = await client.read_gatt_char(CURRENT_BATT)
    return float(battery[0])

async def getTargetTemp(client):
    await checkConnected(client)
    targetTemp = await client.read_gatt_char(TARGET_TEMP)
    return float(int.from_bytes(targetTemp, byteorder='little', signed=False)) * 0.01

async def getCurrentTemp(client):
    await checkConnected(client)
    currentTemp = await client.read_gatt_char(CURRENT_TEMP)
    currentDegree = float(int.from_bytes(currentTemp, byteorder='little', signed=False)) * 0.01
    return currentDegree

async def disconnect(client):
    await client.disconnect()

async def run(address):
    if not address:
        address = await searchForMug()
        if not address:
            print("Could not find a mug :(")
            return

    try:
        async with BleakClient(address) as client:
            print("Current Battery: %f" % await getBattery(client))
            print("Current Temperature: %f" % await getCurrentTemp(client))
            print("Target Temperature: %f" % await getTargetTemp(client))
            print("Liquid State: %s" % await getLiquidState(client))
            print("Liquid Level: %s" % await getLiquidLevel(client))
            await disconnect(client)
    except Exception as exc:
        print('Error: %s' % exc)
        return

if __name__ == "__main__":
    address = None
    if len(sys.argv) > 1:
        address = sys.argv[1]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(address))
